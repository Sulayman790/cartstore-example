import { Component, OnInit } from '@angular/core';
import { CartService } from '../cart.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Product } from '../products';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent {

  public items: Product[];
  public checkoutForm: FormGroup;

  constructor(
    private cartService: CartService,
    private formBuilder: FormBuilder,
  ) {
    this.items = this.cartService.getItems();

    this.checkoutForm = this.formBuilder.group({
      name: ['', Validators.required],
      address: ['']
    });
  }

  onSubmit(customerData) {

    // Process checkout data here
    console.warn('Your order has been submitted', customerData);
    console.log(this.checkoutForm.controls['name'].value);
    this.items = this.cartService.clearCart();
    this.checkoutForm.reset();
  }

}